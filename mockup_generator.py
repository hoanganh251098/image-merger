from win32com.client import Dispatch
from psd_tools import PSDImage
from shutil import copyfile
from PIL import Image
import pythoncom
import time
import os

DESIGN_SIZE = (1000, 1000)

def update_all_smart_objects(app):
    psDisplayNoDialogs = 3
    idplacedLayerUpdateAllModified = app.StringIDToTypeID('placedLayerUpdateAllModified')
    app.ExecuteAction(idplacedLayerUpdateAllModified, None, psDisplayNoDialogs)

def save_jpg(doc, path):
    psStandardBaseline = 1
    psNoMatte = 1
    save_options = Dispatch("Photoshop.JPEGSaveOptions")
    save_options.EmbedColorProfile = True
    save_options.FormatOptions = psStandardBaseline
    save_options.Matte = psNoMatte
    save_options.quality = 10
    doc.SaveAs(SaveIn=path, Options=save_options, AsCopy=True)

def replace_design(psd_path, input_path):
    pythoncom.CoInitialize()
    psd_folder = os.path.normpath(os.path.join(psd_path, '..'))
    design_raw_file_path = os.path.join(psd_folder, 'design_raw.png')
    design_file_path = os.path.join(psd_folder, 'design.png')
    if os.path.exists(design_raw_file_path):
        os.remove(design_raw_file_path)
    if os.path.exists(design_file_path):
        os.remove(design_file_path)
    time.sleep(0.5)
    copyfile(input_path, design_raw_file_path)

    design_image = Image.open(design_raw_file_path)

    design_image_size = design_image.size

    design_image_scale_x = DESIGN_SIZE[0] / design_image_size[0]
    design_image_scale_y = DESIGN_SIZE[1] / design_image_size[1]

    design_image_scale_ratio = min(design_image_scale_x, design_image_scale_y)

    design_image.thumbnail((
        int(design_image_scale_ratio * design_image_size[0]),
        int(design_image_scale_ratio * design_image_size[1])
    ), Image.ANTIALIAS)

    resized_image = Image.new('RGBA', DESIGN_SIZE)

    resized_image.paste(design_image, (int(DESIGN_SIZE[0] / 2 - design_image.size[0] / 2), int(DESIGN_SIZE[1] / 2 - design_image.size[1] / 2)))

    resized_image.save(design_file_path)

def generate_mockup(psd_path, input_path, output_path):
    pythoncom.CoInitialize()
    replace_design(psd_path, input_path)

    app = Dispatch("Photoshop.Application")

    psd_path = os.path.join(os.getcwd(), psd_path)

    doc = app.Open(psd_path)

    update_all_smart_objects(app)

    if os.path.exists(output_path):
        os.remove(output_path)
        time.sleep(0.5)
    save_jpg(doc, os.path.join(os.getcwd(), output_path))
    doc.Save()
    doc.Close()

def generate_design_layer(psd_path, input_path, output_path):
    pythoncom.CoInitialize()
    replace_design(psd_path, input_path)

    app = Dispatch("Photoshop.Application")

    psd_path = os.path.join(os.getcwd(), psd_path)

    doc = app.Open(psd_path)

    for layer in doc.Layers:
        if layer.Name == 'design':
            layer.Visible = True
        elif layer.Name == 'colors':
            layer.Visible = False
        elif layer.Name == 'background':
            layer.Visible = False

    update_all_smart_objects(app)

    if os.path.exists(output_path):
        os.remove(output_path)
        time.sleep(0.5)

    doc.Save()
    doc.Close()

    psd = PSDImage.open(psd_path)
    psd.composite().save(os.path.join(os.getcwd(), output_path))

def generate_base_layer(psd_path, output_folder):
    pythoncom.CoInitialize()

    app = Dispatch("Photoshop.Application")

    psd_path = os.path.join(os.getcwd(), psd_path)

    doc = app.Open(psd_path)

    update_all_smart_objects(app)

    colors_group = None

    for layer in doc.Layers:
        if layer.Name == 'design':
            layer.Visible = False
        elif layer.Name == 'colors':
            layer.Visible = True
            colors_group = layer
        elif layer.Name == 'background':
            layer.Visible = True

    for layer in colors_group.Layers:
        layer.Visible = False

    for layer in colors_group.Layers:
        layer.Visible = True
        doc.Save()

        # save to image
        save_file_path = os.path.join(os.path.join(os.getcwd(), output_folder), f'{layer.Name}.png')
        if os.path.exists(save_file_path):
            os.remove(save_file_path)
            time.sleep(0.5)
        psd = PSDImage.open(psd_path)
        psd.composite().save(save_file_path)

        layer.Visible = False

    doc.Save()
    doc.Close()

