from peewee import *
import datetime

db = SqliteDatabase('database.db');


class BaseModel(Model):
    class Meta:
        database = db

class Psd(BaseModel):
    name        = CharField(unique=True)
    created_at  = DateTimeField(default=datetime.datetime.now)

class Design(BaseModel):
    name        = CharField(unique=True)
    created_at  = DateTimeField(default=datetime.datetime.now)

class PsdAndDesign(BaseModel):
    psd         = ForeignKeyField(Psd, backref='psdanddesigns')
    design      = ForeignKeyField(Design, backref='psdanddesigns')

class Task(BaseModel):
    status      = CharField(default='pending')
    description = TextField()
    result      = TextField()
    created_at  = DateTimeField(default=datetime.datetime.now)

db.connect()
db.create_tables([Psd, Design, PsdAndDesign, Task])



