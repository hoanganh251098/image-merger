import styled from 'styled-components';

export default styled.select`
    padding: 0.25rem 1rem;
    border: 1px solid gray;
    border-radius: 0.25rem;
    line-height: 2rem;
    height: 2rem;
    outline: none !important;

    :focus {
        border-color: #fc7a36;
    }
`;


