import styled from 'styled-components';

import AppButton from './button';

const AppContainer = styled.div`
    border-bottom: 1px solid #eee;
    height: 5rem;
    display: flex;
    align-items: center;
    padding: 0 1rem;
`;

const AppControlContainer = styled.div`
    flex: 1;
    display: flex;
    align-items: center;
    height: 5rem;
    justify-content: space-between;
    padding: 0 0 0 1rem;
`;

function AppHeader(props) {
    const state = useState(globalState);
    const prevViewData = globalData.viewStack[state.viewStackSize.get() - 2];
    const viewData = globalData.viewStack[state.viewStackSize.get() - 1] ?? {};
    const viewTitle = viewData.title ?? '';
    return <AppContainer>
        <AppButton 
            onClick={globalMutation.toggleSidebar}
            >
                <i className="fa fa-bars"></i>
        </AppButton>
        <AppControlContainer>
            <div>
                {prevViewData &&
                    <AppButton theme="light" onClick={globalMutation.popView}>
                        <i className="fa fa-arrow-left"></i>
                        <span style={{margin: '0 0 0 1rem', fontWeight: 'bold'}}>Back</span>
                    </AppButton>
                }
                <span style={{ whiteSpace: 'nowrap' }}>
                    <i className="fa fa-dot-circle" style={{ margin: prevViewData ? '0 0 0 1rem' : '0' }}></i>
                    <span style={{margin: '0 0 0 1rem', fontWeight: 'bold'}}>{viewTitle}</span>
                </span>
            </div>
        </AppControlContainer>
    </AppContainer>
}

export default AppHeader;
