import { useRef } from 'react';
import styled from 'styled-components';

const AppUploadWrapper = styled.div`
    .file-wrapper {
        position: relative;
        width: 100%;
        height: 5rem;
    }

    input {
        display: block;
        width: 100%;
        height: 5rem;
        position: absolute;
        top: 0;
        opacity: 0;
    }

    label {
        display: flex;
        width: 100%;
        height: 5rem;
        border: 1px solid gray;
        border-style:dashed;
        border-radius: 0.25rem;
        justify-content: center;
        align-items: center;
        pointer-events: none;
    }

    .file-wrapper:hover label {
        border-color: #fc7a36;
    }

    .preview-container {
        padding: 1rem;
        width: 100%;
        max-width: 60vw;
        height: 30rem;
    }

    .preview-container:empty {
        padding: 0;
        width: 0;
        height: 0;
    }

    img {
        object-fit: contain;
        width: 100%;
        height: 100%;
    }
`;

const AppPreviewImage = props => {
    return props.src
        ? <img {...props}/>
        : null
        ;
};



export default function(props) {
    const state = useState({
        previewSrc: null
    });
    const fileInputRef = useRef(null);
    const formID = core.uuidv4();
    const handleChange = e => {
        const file = e.target.files[0];
        const previewSrc = URL.createObjectURL(file);
        props.onChange && props.onChange({
            previewURL: previewSrc,
            file: file
        });
        state.previewSrc.set(previewSrc);
    };
    const preventDefault = e => e.preventDefault();
    const handleDrop = e => {
        const fileInput = fileInputRef.current;

        fileInput.files = e.dataTransfer.files;
        const dT = new DataTransfer();
        dT.items.add(e.dataTransfer.files[0]);
        dT.items.add(e.dataTransfer.files[3]);
        fileInput.files = dT.files;

        e.preventDefault();
    }
    return <AppUploadWrapper>
        <div className="file-wrapper">
            <label htmlFor={formID} onDragOver={preventDefault} onDragEnter={preventDefault} onDrop={handleDrop}>
                {props.children}
            </label>
            <input ref={fileInputRef} accept={props.accept} type="file" id={formID} onChange={handleChange}/>
        </div>
        <div className="preview-container">
            <AppPreviewImage src={state.previewSrc.get()}></AppPreviewImage>
        </div>
    </AppUploadWrapper>
};


