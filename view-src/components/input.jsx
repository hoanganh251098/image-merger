import styled from 'styled-components';

export default styled.input.attrs(props => ({
    spellCheck: 'false',
    autoComplete: 'off'
}))`
    padding: .25rem 1rem;
    border: 1px solid gray;
    border-radius: 0.25rem;
    line-height: 2rem;
    height: 2rem;
    outline: none !important;
    position: relative;

    :focus {
        border-color: #fc7a36;
    }
`;


