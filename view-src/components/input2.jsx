import styled from 'styled-components';

const AppInternalInput = styled.input.attrs(props => ({
    spellCheck: 'false',
    autoComplete: 'off'
}))`
    padding: .25rem 0;
    border: none;
    line-height: 2rem;
    width: 100%;
    height: 2rem;
    margin-top: 1rem;
    outline: none !important;
    position: relative;
    transition: .2s;
    background: transparent;
`;

const AppInputWrapper = styled.div`
    position: relative;
    display: flex;
    justify-content: center;

    :before {
        content: "";
        position: absolute;
        bottom: 0;
        display: block;
        width: 100%;
        height: 2px;
        background-color: #eee;
        transition: .2s;
    }

    :after {
        content: "";
        position: absolute;
        bottom: 0;
        display: block;
        ${props => props.focus ? 'width: 100%;' : 'width: 0;'}
        height: 2px;
        background-color: #ff6900;
        transition: .2s;
    }

    label {
        position: absolute;
        left: 0;
        ${props => (props.focus || props.inputValue) ? 'top: 0rem;' : 'top: 1rem;'}
        ${props => (props.focus || props.inputValue) ? 'font-size: .75rem;' : 'font-size: .875rem;'}
        color: gray;
        transition: .2s;
    }
`;

const AppInput = (props) => {
    const state = useState({
        inputValue: '',
        focus: false
    });
    if(props.value && props.value != state.inputValue.get()) {
        state.inputValue.set(props.value);
    }
    const handleFocus = () => {
        state.focus.set(true);
    };
    const handleBlur = () => {
        state.focus.set(false);
    };
    const handleChange = e => {
        state.inputValue.set(e.target.value);
        props.onChange && props.onChange(e);
    };
    return <AppInputWrapper focus={state.focus.get()} inputValue={state.inputValue.get()}>
        <label>{props.label}</label>
        <AppInternalInput {...props} onFocus={handleFocus} onBlur={handleBlur} onChange={handleChange} value={state.inputValue.get()}/>
    </AppInputWrapper>;
};

export default AppInput;
