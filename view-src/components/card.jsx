import styled from 'styled-components';

const AppContainer = styled.div`
    flex: 1;
    background: white;
    border-radius: .2rem;
    border: 1px solid #eee;
    padding: 2rem;
    margin: .5rem;
    overflow: auto;
`;

export default AppContainer;
