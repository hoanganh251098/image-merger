import { useState } from '@hookstate/core';
import styled from 'styled-components';

import AppButton from './button';

const AppContainer = styled.div`
    position: fixed;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    background: #0000005c;
    overflow: hidden;
    ${ props => props.show ? '' : 'display: none;' }
`;

const AppModalContainer = styled.div`
    position: absolute;
    top: 0;
    left: 0;
    display: flex;
    width: 100%;
    height: 100%;
    justify-content: center;
    overflow: hidden;
`;

function AppModal(props) {
    const modalStyle = {
        padding: '2rem',
        background: 'white',
        borderRadius: '.25rem',
        marginTop: '3rem',
        overflow: 'auto',
        minWidth: '40%',
        height: 'fit-content',
        maxHeight: 'calc(100vh - 6rem)'
    };
    return <AppModalContainer>
        <div style={modalStyle} {...props}>
        </div>
    </AppModalContainer>
}

function AppMainContent(props) {
    const state = useState(globalState);
    const modals = [];
    for(let i = 0; i < state.modalStackSize.get(); ++i) {
        modals.push(globalData.modalStack[i]);
    }
    return <AppContainer show={!!state.modalStackSize.get()}>
        {modals.map((modal, idx) => {
            const modalTitle = modal.title;
            const ModalContent = modal.content;
            const closeBtnStyle = {
                borderRadius: '50%',
                width: '2rem',
                height: '2rem',
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center'
            };
            const modalHeader = modalTitle && 
                <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'flex-start' }}>
                    <span style={{ fontWeight: 'bold' }}>{modalTitle}</span>
                    <AppButton onClick={e => globalMutation.closeModal(modal.id)} theme="light" style={closeBtnStyle}>
                        <i className="fa fa-times"></i>
                    </AppButton>
                </div>
                ;
            if(typeof ModalContent == 'function') {
                return <AppModal key={idx}>
                    { modalHeader }
                    <ModalContent data={modal.data}/>
                </AppModal>;
            }
            return <AppModal key={idx}>
                { modalHeader }
                {ModalContent}
            </AppModal>;
        })}
    </AppContainer>
}

export default AppMainContent;
