import { useState } from '@hookstate/core';
import styled from 'styled-components';

import PageDashboard from '../pages/dashboard';
import PageDemo from '../pages/demo';

const AppContainer = styled.div`
    background: white;
    border-right: 1px solid #eee;
    width: 25rem;
    transition: .2s;
    @media (max-width: ${breakpoints.xl}px) {
        width: 20rem;
    }
    @media (max-width: ${breakpoints.lg}px) {
        width: 16rem;
    }
    ${props => props.show ? '' : 'width: 5rem !important;'}
`;

const AppSidebarItemWrapper = styled.div`
    padding: 1rem;
    height: 3rem;
    overflow: hidden;
    display: flex;
    align-items: center;
    position: relative;
    user-select: none;
    cursor: pointer;
    color: #555;
    ${props => props.active ? 'box-shadow: -5px 0px 0px #333 inset;' : ''}
`;

function AppSidebarItem(props) {
    const state = useState(globalState);
    state.viewStackSize.get(); // rerender when view stack changed
    const viewData = globalData.viewStack[0] ?? {};
    const isActive = viewData.title == props.title;
    const logoWrapperStyle = {
        display: 'inline-flex',
        justifyContent: 'center',
        width: '3rem',
        fontSize: '1.5rem',
        color: isActive ? '#ff8181' : '#999'
    };
    const textStyle = {
        display: state.showSidebar.get() ? 'inline-block' : 'none',
        position: 'absolute',
        marginLeft: '4rem',
        maxWidth: 'calc(100% - 7rem)',
        overflow: 'hidden',
        textOverflow: 'ellipsis',
        whiteSpace: 'nowrap',
        fontWeight: 'bold',
        color: isActive ? '#ff8181' : '#999'
    };
    const title = props.title ?? 'Menu item';
    const logo = props.logo ?? 'th-large';
    const logoStyle = props.logoStyle ?? 'fas';
    const handleClick = (e) => {
        state.activeMenuName.set(props.name ?? '');
        props.onClick && props.onClick(e);
    };
    return <AppSidebarItemWrapper {...props} onClick={handleClick} active={isActive}>
        <div style={logoWrapperStyle}>
            <i className={`${logoStyle} fa-${logo}`}></i>
        </div>
        <span style={textStyle}>{title}</span>
    </AppSidebarItemWrapper>
}

function AppSidebar(props) {
    const state = useState(globalState);
    const logoWrapperStyle = {
        height: '5rem',
        borderBottom: '1px solid #eee',
        padding: '1rem',
        display: 'flex'
    };
    const logoInnerWrapperStyle = {
        background: state.showSidebar.get()
            // ? '#666'
            ? 'radial-gradient(circle, rgba(238,174,202,1) 0%, rgba(148,187,233,1) 100%)'
            : 'linear-gradient(90deg, rgba(131,58,180,1) 0%, rgba(253,29,29,1) 50%, rgba(252,176,69,1) 100%)',
        flex: '1',
        borderRadius: '.2rem',
        transition: '.5s',
        cursor: 'pointer'
    };
    const handleLogoClick = (e) => {
        e.preventDefault();
        state.activeMenuName.set('dashboard');
        globalMutation.setView(PageDashboard());
    };
    return <AppContainer show={state.showSidebar.get()}>
        <div style={logoWrapperStyle}>
            <a href="/view" style={logoInnerWrapperStyle} onClick={handleLogoClick} draggable="false">
            </a>
        </div>
        <div>
            <AppSidebarItem title="Dashboard" onClick={e => globalMutation.setView(PageDashboard())}/>
            <AppSidebarItem title="Demo page 1" onClick={e => globalMutation.setView(PageDemo())}/>
            <AppSidebarItem title="Demo page 2" onClick={e => globalMutation.setView({title:'Demo page 2'})}/>
        </div>
    </AppContainer>
}

export default AppSidebar;
