import styled from 'styled-components';

const AppContainer = styled.div`
    height: 2rem;
    display: inline-flex;

    label {
        position: relative;
        display: flex;
        justify-content: center;
        align-items: center;
        width: 2rem;
        height: 2rem;
        border-radius: .25rem;
        background: #333;
    }

    label .indicator {
        display: none;
        width: 1rem;
        height: 1rem;
        color: white;
    }

    input:checked ~ label .indicator {
        display: block;
        position: absolute;
    }

    input {
        display: none;
    }
`;

export default function(props) {
    const inputID = core.uuidv4();
    const scale = props.scale ?? 1;
    return <AppContainer style={{transform:`scale(${scale})`}}>
        <input id={inputID} type="checkbox"/>
        <label htmlFor={inputID}>
            <i className="fa fa-check indicator"></i>
        </label>
    </AppContainer>
};