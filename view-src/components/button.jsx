import styled from 'styled-components';

const lightTheme = `
    background: #eee;
    color: #333;
    :hover {
        background: #ddd;
    }
`;

export default styled.button`
    border: none;
    outline: none;
    border-radius: .25rem;
    background: #333;
    color: #eee;
    padding: .5rem 1rem;
    :hover {
        background: #555;
    }
    cursor: pointer;

    :disabled {
        background: #aaa;
        cursor: not-allowed;
    }

    ${props => props.theme == 'light' ? lightTheme : ''}
`;