import { useState } from '@hookstate/core';
import styled from 'styled-components';

const AppContainer = styled.div`
    flex: 1;
    background: #f2f5f7;
    padding: .5rem;
    overflow: auto;
`;

function AppMainContent(props) {
    const state = useState(globalState);
    const viewData = globalData.viewStack[state.viewStackSize.get() - 1] ?? {};
    const ViewContent = viewData.content;
    return <AppContainer>
        { typeof ViewContent == 'function' ? <ViewContent data={viewData.data}/> : ViewContent }
    </AppContainer>
}

export default AppMainContent;
