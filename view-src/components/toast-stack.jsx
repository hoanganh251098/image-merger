import { useState } from '@hookstate/core';
import styled from 'styled-components';

import AppButton from './button';

const AppContainer = styled.div`
    ${props => props.show ? '' : 'display: none;'}

    position: fixed;
    width: 100%;
    height: 100vh;
    overflow: hidden;
    display: flex;
    flex-direction: column;
    justify-content: flex-end;
    align-items: center;
    padding-bottom: 2rem;
    pointer-events: none;

    .toast {
        border-radius: .25rem;
        padding: 1rem;
        background: black;
        color: white;
        opacity: 0.75;
        margin: .5rem;
    }

    .toast.fade {
        opacity: 0;
        transition: .5s;
    }
`;

function AppMainContent(props) {
    const state = useState(globalState);
    
    return <AppContainer show={!!state.toastStack.get().length}>
        { state.toastStack.map((toastData, idx) => {
            return <div key={idx} className={`toast ${toastData.fade.get() ? 'fade' : ''}`}>
                <span>{toastData.message.get()}</span>
            </div>
        }) }
    </AppContainer>
}

export default AppMainContent;
