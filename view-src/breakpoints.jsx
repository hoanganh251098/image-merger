window.breakpoints = {
    xs: 360,
    sm: 640,
    md: 900,
    lg: 1200,
    xl: 1600
};
