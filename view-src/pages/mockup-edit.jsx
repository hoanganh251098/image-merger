
import AppCard from '../components/card';
import AppButton from '../components/button';
import AppInput from '../components/input2';
import AppCheckbox from '../components/checkbox';

import styled from 'styled-components';
import { useState } from '@hookstate/core';
import axios from 'axios';

const AppContainer = styled.div`
    display: flex;
    justify-content: center;

    img {
        display: block;
        width: 100%;
        object-fit: contain;
    }

    > .card {
        /*
        height: calc(100vh - 7rem);
        justify-content: center;
        */
        flex: unset;
        display: flex;
        align-items: center;
        width: 50%;
        min-width: 30rem;
        max-width: 100%;
    }

    > .card > div {
        flex: 1
    }

    .button-container {
        display: flex;
    }

    .button-container .preview-btn {
        flex: 1;
    }

    .button-container .delete-btn {
        color: #333;
        background: #eee;
    }

    .button-container .delete-btn:hover {
        background: #ff6900;
        color: white;
    }

    .psd-option {
        display: flex;
        align-items: center;
        border: 1px solid transparent;
        border-radius: .25rem;
        padding: 1rem;
        opacity: .5;
        background: #eee;
    }
    
    .psd-option.active {
        opacity: 1;
        background: unset;
    }

    .psd-option:hover {
        opacity: 1;
        border: 1px solid gray;
        background: #e8f0f5;
    }

    .psd-option img {
        display: inline-block;
        object-fit: contain;
        width: 3rem;
        height: 3rem;
        margin-right: 1rem;
    }

    .psd-option .indicator {
        position: relative;
        width: 2rem;
        height: 2rem;
        margin-right: 1rem;
        display: flex;
        justify-content: center;
        align-items: center;
        font-size: 1.25rem;
    }

    .psd-option .indicator i {
        position: absolute;
        display: none;
    }

    .psd-option.active .indicator i {
        display: block;
    }
`;

const AppPsdOption = props => {
    const state = useState({
        active: false
    });
    if(props.active != undefined && props.active != state.active.get()) {
        state.active.set(props.active);
    }
    const active = state.active.get();
    const handleClick = e => {
        props.onChange && props.onChange(!state.active.get());
        state.active.set(p => !p);
    };
    const className = "psd-option" + (active ? " active" : '');
    return <div className={className} onClick={handleClick}>
        <div className="indicator">
            <i className="fa fa-check"></i>
        </div>
        { props.thumbnail && <>
            <img src={props.thumbnail}/>
        </> }
        { props.children }
    </div>
};

const SquareDiv = styled.div`
    position: relative;
    :after {
        content: '';
        padding-bottom: 100%;
        display: block;
    }
    * {
        position: absolute;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
    }
`;

export default function(name) {
    return {
        title: 'Edit design',
        content: function(props) {
            const state = useState({
                left: 49,
                top: 55,
                width: 20,
                height: 27,
                dataVersion: 0,
                psds: [],
                productData: {}
            });
            const designURL = `/v2/designs/${encodeURI(name)}`;
            const imageStyle = {
                maxWidth: '100%',
                maxHeight: '100%',
                objectFit: 'contain'
            };
            const printAreaStyle = {
                width: state.width.get() + '%',
                height: state.height.get() + '%',
                background: '#fffa',
                left: state.left.get() + '%',
                top: state.top.get() + '%',
                backdropFilter: 'blur(1px)',
                borderRadius: '.2rem',
                transform: 'translate(-50%, -50%)',
                border: '1px solid gray'
            };
            const evalPsdsActiveStates = () => {
                const productData = state.productData.get();
                for(const psdData of state.psds) {
                    if(productData.psds.includes(psdData.name.get())) {
                        psdData.active.set(true);
                    }
                }
            }
            const preload = () => {
                const psdsDataURL = `/v2/api/psds`;
                axios.get(psdsDataURL)
                    .then(res => {
                        const data = res.data.data;
                        state.psds.set(res.data.data);
                        evalPsdsActiveStates();
                    })
                    .catch(console.error)
                    ;
            };
            const loadData = () => {
                state.dataVersion.set(p => p + 1);
                const version = state.dataVersion.get();
                const productDataURL = `/v2/api/designs/${name}`;
                const designImageURL = `/v2/designs/${name}.png`;
                axios.get(productDataURL)
                .then(response => {
                    const data = response.data.data;
                    state.productData.set(data);
                    evalPsdsActiveStates();
                })
                .catch(error => {
                    if(error.response) {
                        const response = error.response;
                        console.error(response.data.message);
                    }
                    else {
                        console.log(error);
                    }
                });
            };
            if(state.dataVersion.get() == 0) {
                loadData();
                preload();
            }
            const updateLeft = e => {
                state.left.set(+e.target.value);
            };
            const updateTop = e => {
                state.top.set(+e.target.value);
            };
            const updateWidth = e => {
                state.width.set(+e.target.value);
            };
            const updateHeight = e => {
                state.height.set(+e.target.value);
            };
            const handleDelete = e => {
                const modalID = globalMutation.pushModal({
                    title: 'Are you sure?',
                    data: {
                    },
                    content(props) {
                        const handleNo = () => {
                            globalMutation.closeModal(modalID);
                        };
                        const handleYes = () => {
                            axios.delete(`/v2/api/designs/${encodeURI(name)}`)
                                .then(() => {
                                    globalMutation.closeModal(modalID);
                                    globalMutation.popView();
                                })
                                .catch(console.error);
                        };
                        return <div style={{display:'flex',justifyContent:'flex-end',marginTop:'1rem'}}>
                            <AppButton theme="light" onClick={handleNo}>No, I need to reconsider</AppButton>
                            <div style={{marginLeft:'1rem'}}></div>
                            <AppButton onClick={handleYes}>Yes</AppButton>
                        </div>
                    }
                });
            };
            const handleSave = e => {
                axios.put(`/v2/api/designs/${encodeURI(name)}`, {
                    psds: state.psds.get().filter(v => !!v.active).map(v => v.name)
                })
                    .then(() => {
                        globalMutation.toast({ message: 'Saved' });
                    })
                    .catch(console.error)
                    ;
            };
            return <AppContainer>
                <AppCard className="card">
                    <div>
                        <img src={designURL}/>
                        <div style={{paddingTop:'1rem'}}></div>
                        <span style={{fontWeight:'bold'}}>Design info</span>
                        <AppInput label="Name" type="text" value={name} readOnly/>
                        <div style={{paddingTop:'1rem'}}></div>
                        <span style={{fontWeight:'bold'}}>Print area config</span>
                        {
                            state.psds.get().map((psdData, idx) => {
                                const isActive = (state.productData.psds.get() ?? []).includes(psdData.name);
                                return <AppPsdOption key={idx} thumbnail={designURL} active={state.psds[idx].active.get() ?? isActive} onChange={v => state.psds[idx].active.set(v)}>{psdData.name}</AppPsdOption>
                            })
                        }
                        <div>
                            <AppInput label="Left" type="number" value={state.left.get()} onChange={updateLeft} />
                            <AppInput label="Top" type="number" value={state.top.get()} onChange={updateTop} />
                            <AppInput label="Width" type="number" value={state.width.get()} onChange={updateWidth} />
                            <AppInput label="Height" type="number" value={state.height.get()} onChange={updateHeight} />
                            <AppCheckbox/>
                            <div style={{paddingTop:'1rem'}}></div>
                            <div style={{paddingTop:'1rem'}}></div>
                            <div className="button-container">
                                <AppButton className="delete-btn" onClick={handleDelete}>
                                    <i className="fa fa-trash"></i>
                                </AppButton>
                                <div style={{paddingLeft:'1rem'}}></div>
                                <AppButton theme="light" className="preview-btn">
                                    Preview
                                </AppButton>
                                <div style={{paddingLeft:'1rem'}}></div>
                                <AppButton className="save-btn" onClick={handleSave}>
                                    <i className="fa fa-save"></i>
                                    &nbsp;
                                    Save
                                </AppButton>
                            </div>
                        </div>
                    </div>
                </AppCard>
                {/* <AppCard style={{flex: '1', height: 'calc(100vh - 7rem)', display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
                    <SquareDiv style={{ maxWidth: 'calc(60vh - 7rem)', width: '80%', overflow: 'hidden' }}>
                        <div style={{ width: '100%' }}>
                            <img src={`/base-mockups/${name}`} alt="Mockup" style={imageStyle} draggable="false"/>
                        </div>
                        <div style={printAreaStyle}></div>
                    </SquareDiv>
                </AppCard> */}
            </AppContainer>
        }
    }
}