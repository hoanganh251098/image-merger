
import AppCard from '../components/card';
import AppButton from '../components/button';
import AppInput from '../components/input2';
import AppSelect from '../components/select';
import AppUpload from '../components/upload';

import PageMockupEdit from './mockup-edit';
import { useState } from '@hookstate/core';

import styled from 'styled-components';

import axios from 'axios';

const StyledTable = styled.table`
    width: 100%;
    border-collapse: collapse;
    th {
        text-align: left;
    }
    td {
        padding: 1rem 0;
    }
    tr {
        border-bottom: 1px solid gray;
    }
`;

const AppImageContainer = styled.div`
    position: relative;
    display: flex;
    justify-content: center;
    align-items: center;
    width: 15rem;
    height: 20rem;
    padding: 1rem;
    margin: 1rem;
    img {
        ${props => props.hide ? 'display: none;' : ''}
    }
    div {
        position: absolute;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        background: #7a7a7a2b;
        border-radius: .5rem;
        cursor: pointer;
        z-index: 0;
    }
`;

const AppPaginationWrapper = styled.div`
    display: flex;
    justify-content: center;

    .page:not(.current-page) {
        background-color: transparent;
        color: #333;
    }
    .page:not(.current-page, .page-ellipsis):hover {
        background-color: #ff6900;
        color: white;
    }
    .page-ellipsis {
        color: #777;
    }
    .prev-page:disabled, .next-page:disabled {
        background-color: transparent;
        color: #777;
        cursor: default;
    }
    .page:disabled {
        cursor: default;
    }
`;

export default function() {
    return {
        title: 'Dashboard',
        data: {
            tableData: [],
            filter: {
                page: 1,
                pageSize: 12,
                searchString: '',
                total: 0,
                showing: {
                    min: 0,
                    max: 0
                }
            },
            dataVersion: 0,
            hiddenImages: []
        },
        content: function(props) {
            const state = useState(props.data);
            const loadTableData = () => {
                state.tableData.set([]);
                state.hiddenImages.set([]);
                state.dataVersion.set(p => p + 1);
                const version = state.dataVersion.get();
                axios.get(`/v2/api/designs?page=${state.filter.page.get()}&page_size=${state.filter.pageSize.get()}`)
                .then(response => {
                    const designs = response.data.data;
                    if(state.dataVersion.get() != version) return;
                    state.tableData.set(designs);
                    state.filter.total.set(response.data.total);
                });
            };
            if(state.dataVersion.get() == 0) {
                loadTableData();
            }
            const maxPages = () => Math.floor(state.filter.total.get() / state.filter.pageSize.get()) + 1;
            let fileToUpload = null;
            const showCreateModal = () => {
                const modalID = globalMutation.pushModal({
                    title: 'Create new design',
                    data: {
                        name: '',
                        errorMessage: '',
                        uploadMessage: 'Upload your design here',
                        previewURL: null,
                        file: null,
                        buttonMessage: '',
                        buttonDisabled: false
                    },
                    content(props) {
                        const state = useState(props.data);
                        const handleChange = e => {
                            state.errorMessage.set('');
                            state.name.set(e.target.value);
                        };
                        const handleCreate = () => {
                            const mockupName = state.name.get();
                            const previewURL = state.previewURL.get();
                            const file = fileToUpload;
                            if(mockupName == '') {
                                return state.errorMessage.set('Name is required');
                            }
                            if(!previewURL) {
                                return state.errorMessage.set('Design is required')
                            }
                            const handleUpload = ({create}) => {
                                state.buttonDisabled.set(true);
                                if(create) {
                                    axios.post(`/v2/api/designs`, {
                                        name: mockupName
                                    })
                                        .then(() => handleUpload({create: false}))
                                        .catch(error => {
                                            const response = error.response;
                                            state.errorMessage.set(response.data.message);
                                            state.buttonDisabled.set(false);
                                        })
                                        ;
                                    return;
                                }
                                try {
                                    const formData = new FormData();
                                    formData.append('file', file);
                                    formData.append('name', mockupName);
                                    axios({
                                        method: 'post',
                                        url: '/v2/api/design_upload',
                                        data: formData,
                                        headers: {
                                            'Content-Type': 'multipart/form-data'
                                        }
                                    })
                                        .then(response => {
                                            state.buttonDisabled.set(false);
                                            globalMutation.closeModal(modalID);
                                            loadTableData();
                                            // globalMutation.pushView(PageMockupEdit());
                                        })
                                        .catch(error => {
                                            const response = error.response;
                                            state.errorMessage.set(response.data.message);
                                            state.buttonDisabled.set(false);
                                        })
                                }
                                catch(error) {
                                    console.log(error);
                                }
                            };
                            axios.get(`/v2/api/designs/${encodeURI(mockupName)}`)
                                .then(() => handleUpload({create: true}))
                                .catch(() => handleUpload({create: true}))
                                ;
                        };
                        const handleFileChange = data => {
                            state.uploadMessage.set('Change image');
                            state.previewURL.set(data.previewURL);
                            fileToUpload = data.file;
                            const name = data.file.name.replace('.png', '');
                            if(!state.name.get()) {
                                state.name.set(name);
                            }
                        };
                        return <>
                            {
                                state.errorMessage.get() &&
                                <>
                                    <div style={{paddingTop:'1rem'}}></div>
                                    <span style={{ color: '#ff6335', fontSize: '.9rem' }}>
                                        <i className="fa fa-exclamation-circle"></i>
                                        &nbsp;
                                        {state.errorMessage.get()}
                                    </span>
                                    <div style={{paddingTop:'1rem'}}></div>
                                </>
                            }
                            <AppInput value={state.name.get()} onChange={handleChange} label="design-name"></AppInput>
                            <div style={{marginTop:'1rem'}}></div>
                            <AppUpload accept="image/png" onChange={handleFileChange}>
                                <span>{state.uploadMessage.get()}</span>
                            </AppUpload>
                            <div style={{marginTop:'1rem'}}></div>
                            <AppButton style={{ width: '100%' }} onClick={handleCreate} disabled={state.buttonDisabled.get()}>Create</AppButton>
                        </>
                    }
                });
            };
            const handlePageSizeChange = e => {
                state.filter.pageSize.set(+e.target.value);
                loadTableData();
            };
            const generatePagination = (page, maxPage) => {
                const leftPart = [];
                const rightPart = [];
                if(page <= 4) {
                    for(let i = 1; i < page; ++i) {
                        leftPart.push(i);
                    }
                }
                else {
                    leftPart.push(1);
                    leftPart.push(null);
                    leftPart.push(page - 1);
                }
                if(page >= maxPage - 3) {
                    for(let i = page + 1; i <= maxPage; ++i) {
                        rightPart.push(i);
                    }
                }
                else {
                    rightPart.push(page + 1);
                    rightPart.push(null);
                    rightPart.push(maxPage);
                }
                return [...leftPart, page, ...rightPart];
            };
            const handlePrevPage = () => {
                if(state.filter.page.get() > 1) {
                    state.filter.page.set(p => p - 1);
                    loadTableData();
                }
            };
            const handleNextPage = () => {
                if(state.filter.page.get() < maxPages()) {
                    state.filter.page.set(p => p + 1);
                    loadTableData();
                }
            };
            const handleMoveToPage = page => {
                state.filter.page.set(page);
                loadTableData();
            };
            return <AppCard style={{height: 'calc(100% - 1rem)'}}>
                <div style={{display: 'flex', justifyContent: 'space-between', alignItems: 'center', flexWrap: 'wrap', marginBottom: '2rem'}}>
                    <div style={{display: 'flex'}}>
                        <AppSelect onChange={handlePageSizeChange} value={state.filter.pageSize.get()} style={{height:'3rem'}}>
                            <option value="12">12 items per page</option>
                            <option value="24">24 items per page</option>
                            <option value="60">60 items per page</option>
                            <option value="120">120 items per page</option>
                        </AppSelect>
                        <span style={{marginLeft: '2rem'}}></span>
                        <AppInput type="text" label="Search for design" value={state.filter.searchString.get()} onChange={e => state.filter.searchString.set(e.target.value)} />
                    </div>
                    <div>
                        <AppButton onClick={e => loadTableData()} theme="light">
                            <i className="fa fa-sync"></i>
                        </AppButton>
                        <AppButton onClick={showCreateModal} theme="light" style={{ marginLeft: '1rem' }}>
                            <i className="fa fa-plus"></i>
                        </AppButton>
                    </div>
                </div>
                <div style={{display: 'flex', flexWrap: 'wrap'}}>
                    {
                        state.tableData.get()
                            .filter(data => {
                                return core.quickMatch(data.name, state.filter.searchString.get());
                            })
                            .map((data, idx) => {
                                const mockupURL = `/v2/designs/${encodeURI(data.name)}.png`;
                                const shouldHide = state.hiddenImages.get().includes(mockupURL);
                                const imgContainerStyle = {
                                    display: 'flex',
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    width: '15rem',
                                    height: '20rem',
                                    borderRadius: '.5rem',
                                    margin: '1rem',
                                };
                                const imageStyle = {
                                    objectFit: 'contain',
                                    maxWidth: '100%',
                                    maxHeight: '100%',
                                };
                                if(shouldHide) {
                                    imgContainerStyle.background = '#7a7a7a2b';
                                    imageStyle.display = 'none';
                                }
                                const handleError = () => {
                                    if(!state.hiddenImages.get().includes(mockupURL)) {
                                        state.hiddenImages.set(p => [...p, mockupURL]);
                                    }
                                };
                                const handleClick = () => {
                                    globalMutation.pushView(PageMockupEdit(data.name));
                                };
                                return <div key={idx}>
                                    <AppImageContainer>
                                        <img src={mockupURL} style={imageStyle} onError={handleError}/>
                                        <div onClick={handleClick}></div>
                                    </AppImageContainer>
                                    <span style={{display:'flex',justifyContent:'center',alignItems:'center',overflow:'hidden',width:'15rem',whiteSpace:'nowrap',textOverflow:'ellipsis',margin:'1rem'}} title={data.name}>{data.name}</span>
                                </div>;
                            })
                    }
                </div>
                <div style={{padding:'1rem'}}>
                    <span style={{fontSize:'.85rem'}}>Showing {(state.filter.page.get() - 1) *state.filter.pageSize.get() + 1} - {Math.min(state.filter.page.get() * state.filter.pageSize.get(), state.filter.total.get())} of total {state.filter.total.get()} designs</span>
                </div>
                <AppPaginationWrapper className="pagination">
                    <AppButton className="prev-page" onClick={handlePrevPage} disabled={state.filter.page.get() == 1}>Prev</AppButton>
                    <div style={{paddingLeft:'1rem'}}></div>
                    { generatePagination(state.filter.page.get(), maxPages()).map(page => {
                        if(page == null) {
                            return <div key={core.uuidv4()} style={{display:'flex'}}>
                                <AppButton className="page page-ellipsis" disabled>...</AppButton>
                                <div style={{paddingLeft:'1rem'}}></div>
                            </div>
                        }
                        return <div key={core.uuidv4()} style={{display:'flex'}}>
                            <AppButton className={`${state.filter.page.get()==page ? 'page current-page' : 'page'}`}
                                disabled={state.filter.page.get() == page}
                                onClick={e => handleMoveToPage(page)}
                                >
                                    {page}
                            </AppButton>
                            <div style={{paddingLeft:'1rem'}}></div>
                        </div>
                    }) }
                    <AppButton className="next-page" onClick={handleNextPage} disabled={state.filter.page.get() == maxPages()}>Next</AppButton>
                </AppPaginationWrapper>
            </AppCard>
        }
    }
}