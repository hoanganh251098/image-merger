
import AppCard from '../components/card';
import AppButton from '../components/button';

import PageMockupEdit from './mockup-edit';
import { useState } from '@hookstate/core';

import axios from 'axios';

export default function() {
    return {
        title: 'Demo page 1',
        content: function(props) {
            const state = useState({
                message: ''
            });
            const setMessage = (msg) => {
                state.message.set(msg);
                setTimeout(() => {
                    state.message.set('')
                }, 1000);
            };
            const sendDemoRequest = () => {
                axios.post('/api/mockups', {
                    name: 'toddler',
                    left: 0.2,
                    top: 0.3,
                    width: 0.3,
                    height: 0.3
                }).then(response => {
                    console.log(response);
                    setMessage('Request sent');
                }).catch(({response}) => {
                    console.log(response);
                    setMessage('Request error');
                });
            };
            return <AppCard style={{height: 'calc(100% - 1rem)', display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
                <div>
                    <AppButton onClick={sendDemoRequest}>Send demo request</AppButton>
                    <p>{state.message.get()}</p>
                    <h4>Nothing to see here, <AppButton onClick={e => globalMutation.pushView( PageMockupEdit())}>edit demo mockup.</AppButton></h4>
                </div>
            </AppCard>
        }
    }
}