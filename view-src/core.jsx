window.core = {
    quickMatch(target, searchStr) {
        let idx = -1;
        for(let c of searchStr) {
            while(idx < target.length) {
                idx++;
                if(c == target[idx]) {
                    break;
                }
            }
            if(idx == target.length) return false;
        }
        return true;
    },
    uuidv4() {
        return (() => (typeof crypto == 'undefined' || crypto.getRandomValues == undefined)
            ? 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, c => {
                let r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
                return r.toString(16); })
            : ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
                (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16))
            )();
    }
};
