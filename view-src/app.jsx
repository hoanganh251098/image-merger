import './core';
import './global-state';
import './global-mutation';
import './breakpoints';
import React from 'react';
import ReactDOM from 'react-dom';
import styled from 'styled-components';
import { useState } from '@hookstate/core';

window.React = React;
window.useState = useState;

import AppSidebar from './components/sidebar';
import AppHeader from './components/header';
import AppMainContent from './components/main-content';
import AppModalStack from './components/modal-stack';
import AppToastStack from './components/toast-stack';

import PageDashboard from './pages/dashboard';

const AppContainer = styled.div`
    display: flex;
    align-items: stretch;
    height: 100%;
`;

const AppMainContentWrapper = styled.div`
    flex: 1;
    display: flex;
    flex-direction: column;
    align-items: stretch;
`;

function App() {
    return <AppContainer>
        <AppSidebar/>
        <AppMainContentWrapper>
            <AppHeader/>
            <AppMainContent/>
        </AppMainContentWrapper>
        <AppModalStack/>
        <AppToastStack/>
    </AppContainer>
}

globalMutation.setView(PageDashboard());

ReactDOM.render(<App/>, document.querySelector('#root'));

