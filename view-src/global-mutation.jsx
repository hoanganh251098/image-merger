const globalMutation = {
    toggleSidebar() {
        globalState.showSidebar.set(!globalState.showSidebar.get());
    },
    pushView(view) {
        globalData.viewStack.push(view);
        globalState.viewStackSize.set(p => p + 1);
    },
    popView() {
        globalState.viewStackSize.set(p => p - 1);
        globalData.viewStack.pop();
    },
    setView(view) {
        globalData.viewStack = view ? [view] : [];
        globalState.viewStackSize.set(view ? 1 : 0);
    },
    pushModal(modal) {
        const modalID = core.uuidv4();
        globalData.modalStack.push({
            ...modal,
            id: modalID
        });
        globalState.modalStackSize.set(p => p + 1);
        return modalID;
    },
    popModal() {
        globalState.modalStackSize.set(p => p + 1);
        globalData.modalStack.pop();
    },
    closeModal(id) {
        let idx = 0;
        for(const modalData of globalData.modalStack) {
            if(modalData.id == id) {
                break;
            }
            idx++;
        }
        globalData.modalStack.splice(idx, 1);
        globalState.modalStackSize.set(globalData.modalStack.length);
    },
    toast({ type='info', message='', duration=3000 }={}) {
        const toastID = core.uuidv4();
        const data = {
            id: toastID,
            type, message,
            fade: false
        };
        globalState.toastStack.set(stack => [...stack, data]);
        setTimeout(() => {
            let toastData = null;
            for(const data of globalState.toastStack) {
                if(data.id.get() == toastID) {
                    toastData = data;
                    break;
                }
            }
            toastData.fade.set(true);
            setTimeout(() => {
                globalState.toastStack.set(stack => stack.filter(toastData => toastData.id != toastID));
            }, 1000);
        }, duration);
    }
};
window.globalMutation = globalMutation;
