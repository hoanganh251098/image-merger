import './core';
import './breakpoints';
import React from 'react';
import ReactDOM from 'react-dom';
import styled from 'styled-components';
import { createState, useState } from '@hookstate/core';
import backgroundImage from './images/login-background.png';
import axios from 'axios';

window.React = React;
window.useState = useState;

window.globalState = createState({
    username: '',
    password: '',
    errorMessage: '',
    isAuthenticating: false
});

const theme = {
    borderRadius: '.25rem'
};

const AppContainer = styled.div`
    position: fixed;
    top: 0;
    left: 0;
    display: flex;
    align-items: stretch;
    width: 100%;
    height: 100%;
    padding: 2rem;
    justify-content: center;
    align-items: center;
    background-color: #eee;

    #background-image {
        display: block;
        position: fixed;
        width: 100%;
        bottom: 0;
        left: 0;
        z-index: 0;
    }

    .content-wrapper {
        border-radius: ${theme.borderRadius};
        width: 35rem;
        height: 25rem;
        padding: 3rem;
        max-height: 90vh;
        display: flex;
        justify-content: center;
        align-items: center;
        background: #fffe;
        backdrop-filter: blur(10px);
        transition: .2s;
        z-index: 1;

        > div {
            flex: 1;
            display: flex;
            flex-direction: column;
            align-items: stretch;
            height: 100%;

            > div {
                flex: 1;
                display: flex;
                flex-direction: column;
                justify-content: center;
                align-items: stretch;
            }
        }
    }

    .divider {
        padding-top: 1rem;
    }
`;


const AppInternalInput = styled.input.attrs(props => ({
    spellCheck: 'false',
    autoComplete: 'off'
}))`
    padding: .25rem 0;
    border: none;
    line-height: 2rem;
    width: 100%;
    height: 2rem;
    outline: none !important;
    position: relative;
    transition: .2s;
    background: transparent;
`;

const AppInputWrapper = styled.div`
    position: relative;
    display: flex;
    justify-content: center;

    :before {
        content: "";
        position: absolute;
        bottom: 0;
        display: block;
        width: 100%;
        height: 2px;
        background-color: #eee;
        transition: .2s;
    }

    :after {
        content: "";
        position: absolute;
        bottom: 0;
        display: block;
        ${props => props.focus ? 'width: 100%;' : 'width: 0;'}
        height: 2px;
        background-color: #ff6900;
        transition: .2s;
    }

    label {
        position: absolute;
        left: 0;
        ${props => (props.focus || props.inputValue) ? 'top: -1rem;' : 'top: 0;'}
        ${props => (props.focus || props.inputValue) ? 'font-size: .75rem;' : 'font-size: .875rem;'}
        color: gray;
        transition: .2s;
    }
`;

const AppInternalLoginButton = styled.button`
    width: 100%;
    height: 4rem;
    border: none;
    outline: none;
    border-radius: .25rem;
    ${props => props.isAuthenticating ? 'background: #aaa !important;' : 'background: #333;'}
    color: #eee;
    font-size: 1.15rem;
    font-weight: bold;
    padding: .5rem 1rem;
    :hover {
        background: #555;
    }
    cursor: pointer;

    @keyframes spin {
        from {
            transform: rotate(0deg);
        }
        to {
            transform: rotate(360deg);
        }
    }

    span {
        display: block;
        ${props => props.isAuthenticating ? 'animation: spin 2s infinite linear;' : ''}
    }
`;

const AppLoginButton = (props) => {
    const state = useState(globalState);
    const handleClick = e => {
        !state.isAuthenticating.get() && props.onClick && props.onClick(e);
    };
    return <AppInternalLoginButton isAuthenticating={state.isAuthenticating.get()} {...props} onClick={handleClick}/>
};

const AppInput = (props) => {
    const state = useState({
        inputValue: props.value ?? '',
        focus: false
    });
    const handleFocus = () => {
        state.focus.set(true);
    };
    const handleBlur = () => {
        state.focus.set(false);
    };
    const handleChange = e => {
        state.inputValue.set(e.target.value);
        props.onChange && props.onChange(e);
    };
    return <AppInputWrapper focus={state.focus.get()} inputValue={state.inputValue.get()}>
        <label>{props.label}</label>
        <AppInternalInput {...props} onFocus={handleFocus} onBlur={handleBlur} onChange={handleChange} value={state.inputValue.get()}/>
    </AppInputWrapper>;
};

function AppLoginForm(props) {
    const state = useState(globalState);

    const handleLogin = () => {
        state.isAuthenticating.set(true);
        state.errorMessage.set('');
        const username = state.username.get();
        const password = state.password.get();
        axios.post('/v2/authenticate', {
            username, password
        }).then(res => {
            const urlSearchParams = new URLSearchParams(window.location.search);
            const params = Object.fromEntries(urlSearchParams.entries());
            const backref = params.backref ?? '/view';
            window.location = backref;
            state.isAuthenticating.set(false);
        }).catch(err => {
            console.log(err.response);
            const res = err.response;
            const message = res.data.message;
            state.errorMessage.set(message);
            state.isAuthenticating.set(false);
        });
    };

    return <div>
        { state.errorMessage.get()
            ? <div>
                <span style={{color:'#ff6900'}}>
                    <i className="fa fa-exclamation-triangle" style={{marginRight: '1rem'}}></i>
                    {state.errorMessage.get()}
                </span>
                <div className="divider"></div>
                <div className="divider"></div>
            </div>
            : null
        }
        <div>
            <AppInput label="Username"
                onChange={e => state.username.set(e.target.value)} value={state.username.get()}/>
            <div className="divider"></div>
            <div className="divider"></div>
            <AppInput label="Password" type="password"
                onChange={e => state.password.set(e.target.value)} value={state.password.get()}
                onKeyUp={e => e.code == 'Enter' && handleLogin()}/>
            <div className="divider"></div>
            <div className="divider"></div>
        </div>
        <AppLoginButton onClick={handleLogin}>
            <span>
                { state.isAuthenticating.get()
                    ? <i className="fa fa-sync-alt"></i>
                    : 'Login'
                }
            </span>
        </AppLoginButton>
    </div>
}

function App() {
    return <AppContainer>
        <img id="background-image" src={backgroundImage}/>
        <div className="content-wrapper">
            <AppLoginForm/>
        </div>
    </AppContainer>
}

ReactDOM.render(<App/>, document.querySelector('#root'));

