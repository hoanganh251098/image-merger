import { createState } from '@hookstate/core';

const globalState = createState({
    showSidebar: false,
    viewStackSize: 0,
    activeMenuName: 'dashboard',
    modalStackSize: 0,
    toastStack: [],
    cacheVersion: +new Date
});
window.globalState = globalState;
const globalData = {
    viewStack: [],
    modalStack: []
};
window.globalData = globalData;
