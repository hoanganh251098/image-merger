const Model = require('./level-model');

function uuidv4() {
    return (() => (typeof crypto == 'undefined' || crypto.getRandomValues == undefined)
        ? 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, c => {
            let r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return r.toString(16); })
        : ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
            (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16))
        )();
}

async function main() {
    const demoModel = Model('demo/people');
    const ha = await demoModel.findOne(v => v.name == 'Hoang Anh');
    if(!ha) {
        await demoModel.create({
            name: 'Hoang Anh',
            age: 0
        });
    }
    else {
        if(ha.age > +ha.__id) {
            await demoModel.remove(ha.__id);
        }
        else {
            await demoModel.update(ha.__id, {
                ...ha,
                age: ha.age + 1
            });
        }
    }
    console.log(await demoModel.findOne(v => v.name == 'Hoang Anh'));
    demoModel.debug();
}
main();

