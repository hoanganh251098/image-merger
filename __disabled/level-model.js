const levelup = require('levelup');
const leveldown = require('leveldown');

function Model(name) {
    const db = levelup(leveldown(`./database/${name}`));
    const get = async (key, defaultValue) => {
        try {
            return (await db.get(key)).toString();
        }
        catch(e) {
            return defaultValue;
        }
    };
    const set = async (key, value) => {
        if(value != null && value != undefined) {
            return await db.put(key, value.toString());
        }
    };
    const idToString = (id) => {
        return ('0000000000' + id).slice(-9);
    };
    return {
        async create(data) {
            const id = +(await get('__index', 0));
            if(data != null && data != undefined) {
                await set('__index', id + 1);
                await db.put(idToString(id), JSON.stringify(data));
                return idToString(id);
            }
        },
        async find(matcher=()=>true, {gt, gte, lt, lte, limit, reverse}={}) {
            const result = [];
            const condition = {};
            if(gt != undefined) condition.gt = gt;
            if(gte != undefined) condition.gte = gte;
            if(lt != undefined) condition.lt = lt;
            if(lte != undefined) condition.lte = lte;
            if(reverse != undefined) condition.reverse = reverse;
            for await (const [k, v] of db.iterator(condition)) {
                if(limit != undefined && result.length >= limit) break;
                const key = k.toString();
                const value = JSON.parse(v.toString());
                const isMatch = matcher(value);
                if(isMatch) {
                    result.push({
                        ...value,
                        __id: key.split('/')[0]
                    });
                }
            }
            return result;
        },
        async findOne(matcher=()=>true, {gt, gte, lt, lte, reverse}={}) {
            const condition = {};
            if(gt != undefined) condition.gt = gt;
            if(gte != undefined) condition.gte = gte;
            if(lt != undefined) condition.lt = lt;
            if(lte != undefined) condition.lte = lte;
            if(reverse != undefined) condition.reverse = reverse;
            for await (const [k, v] of db.iterator(condition)) {
                const key = k.toString();
                const value = JSON.parse(v.toString());
                const isMatch = matcher(value);
                if(isMatch) {
                    return {
                        ...value,
                        __id: key.split('/')[0]
                    };
                }
            }
        },
        async update(id, data) {
            return await db.put(idToString(id), JSON.stringify(data));
        },
        async remove(id) {
            return await db.del(idToString(id));
        },
        get db() {
            return db;
        },
        async debug() {
            console.log('-------- ALL RECORDS --------');
            for await (const [key, value] of db.iterator()) {
              console.log(`${key.toString()}=${value.toString()}`);
            }
        }
    }
}

module.exports = Model;