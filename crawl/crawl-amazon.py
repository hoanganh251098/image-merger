import requests
import re
import json
import sys
from urllib.parse import unquote
from pathlib import Path
from PIL import Image

def safe_list_get (l, idx, default):
  try:
    return l[idx]
  except IndexError:
    return default

url = safe_list_get(sys.argv, 1, input('url: '))
name = safe_list_get(sys.argv, 2, input('name: '))

headers = {
    'dnt': '1',
    'upgrade-insecure-requests': '1',
    'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36',
    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    'sec-fetch-site': 'same-origin',
    'sec-fetch-mode': 'navigate',
    'sec-fetch-user': '?1',
    'sec-fetch-dest': 'document',
    'referer': 'https://www.amazon.com/',
    'accept-language': 'en-GB,en-US;q=0.9,en;q=0.8',
}

res = requests.get(url, headers=headers)

source = res.text
images = [
    m[9:-1].replace('.0_AC_UL1500_.png', '.0_AC_SX2000._SX._UX._SY._UY_.png')
    for m in re.findall(r'"hiRes"\:"[a-zA-Z0-9\.\%\/\:_-]*"', source)
]

images = [*filter((lambda x: '_CLa' in x), images)]

save_data = []

def download_image(image_url, save_path):
    img_data = requests.get(image_url).content
    with open(save_path, 'wb') as handler:
        handler.write(img_data)

def create_folder(path):
    Path(path).mkdir(parents=True, exist_ok=True)

def crop_transparent(image_path, save_path):
    im = Image.open(image_path)
    im2 = im.crop(im.getbbox())
    im2.save(save_path)

download_folder = f'../designs'
create_folder(download_folder)

count = 1
for image in images:
    unquoted = unquote(image)
    match = re.search(r'\|[^\|]*\.png\|', unquoted)
    if not match:
        continue
    image_file_name = match.group().replace('|', '')
    design_image_url = 'https://m.media-amazon.com/images/I/' + image_file_name
    if design_image_url not in save_data:
        save_data.append(design_image_url)
        image_save_path = download_folder + f'/{name}_{count}.png'
        download_image(design_image_url, image_save_path)
        crop_transparent(image_save_path, image_save_path)
        count += 1

