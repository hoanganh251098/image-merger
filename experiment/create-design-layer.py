from win32com.client import Dispatch
from psd_tools import PSDImage
from shutil import copyfile
from PIL import Image
import time
import os

DESIGN_SIZE = (1000, 1000)

def generate_mockup(input_name, output_name):
    if os.path.exists('./design_raw.png'):
        os.remove('./design_raw.png')
    if os.path.exists('./design.png'):
        os.remove('./design.png')
    if os.path.exists(output_name):
        os.remove(output_name)
    time.sleep(0.5)
    copyfile(input_name, './design_raw.png')

    design_image = Image.open('./design_raw.png')

    design_image_size = design_image.size

    design_image_scale_x = DESIGN_SIZE[0] / design_image_size[0]
    design_image_scale_y = DESIGN_SIZE[1] / design_image_size[1]

    design_image_scale_ratio = min(design_image_scale_x, design_image_scale_y)

    design_image.thumbnail((
        int(design_image_scale_ratio * design_image_size[0]),
        int(design_image_scale_ratio * design_image_size[1])
    ), Image.ANTIALIAS)

    resized_image = Image.new('RGBA', DESIGN_SIZE)

    resized_image.paste(design_image, (int(DESIGN_SIZE[0] / 2 - design_image.size[0] / 2), int(DESIGN_SIZE[1] / 2 - design_image.size[1] / 2)))

    resized_image.save('./design.png')

    app = Dispatch("Photoshop.Application")

    psd_file_path = os.getcwd() + '/mockup.psd'

    doc = app.Open(psd_file_path)

    def update_all_smart_objects():
        psDisplayNoDialogs = 3
        idplacedLayerUpdateAllModified = app.StringIDToTypeID('placedLayerUpdateAllModified')
        app.ExecuteAction(idplacedLayerUpdateAllModified, None, psDisplayNoDialogs)

    update_all_smart_objects()

    psStandardBaseline = 1
    psNoMatte = 1
    save_options = Dispatch("Photoshop.JPEGSaveOptions")
    save_options.EmbedColorProfile = True
    save_options.FormatOptions = psStandardBaseline
    save_options.Matte = psNoMatte
    save_options.quality = 10

    doc.SaveAs(SaveIn=os.path.join(os.getcwd(), output_name), Options=save_options, AsCopy=True)
    doc.Save()
    # doc.Close()

input_files = os.listdir('./input')
for file in input_files:
    output_name = f'./output/{file.replace(".png", ".jpg")}'
    if not os.path.exists(output_name):
        generate_mockup(f'./input/{file}', output_name)

def replace_design(design_path):
    if os.path.exists('./design_raw.png'):
        os.remove('./design_raw.png')
    if os.path.exists('./design.png'):
        os.remove('./design.png')
    if os.path.exists(output_name):
        os.remove(output_name)
    time.sleep(0.5)
    copyfile(design_path, './design_raw.png')

    design_image = Image.open('./design_raw.png')

    design_image_size = design_image.size

    design_image_scale_x = DESIGN_SIZE[0] / design_image_size[0]
    design_image_scale_y = DESIGN_SIZE[1] / design_image_size[1]

    design_image_scale_ratio = min(design_image_scale_x, design_image_scale_y)

    design_image.thumbnail((
        int(design_image_scale_ratio * design_image_size[0]),
        int(design_image_scale_ratio * design_image_size[1])
    ), Image.ANTIALIAS)

    resized_image = Image.new('RGBA', DESIGN_SIZE)

    resized_image.paste(design_image, (int(DESIGN_SIZE[0] / 2 - design_image.size[0] / 2), int(DESIGN_SIZE[1] / 2 - design_image.size[1] / 2)))

    resized_image.save('./design.png')

def update_all_smart_objects(app):
    psDisplayNoDialogs = 3
    idplacedLayerUpdateAllModified = app.StringIDToTypeID('placedLayerUpdateAllModified')
    app.ExecuteAction(idplacedLayerUpdateAllModified, None, psDisplayNoDialogs)

def test():
    replace_design('../designs/stellwagen-bank_1.png')

    app = Dispatch("Photoshop.Application")
    psd_file_path = os.getcwd() + '/mockup.psd'
    doc = app.Open(psd_file_path)

    design_layer        = None
    colors_group        = None
    background_group    = None

    for layer in doc.Layers:
        if layer.Name == 'design':
            design_layer = layer
        elif layer.Name == 'colors':
            colors_group = layer
        elif layer.Name == 'background':
            background_group = layer

    update_all_smart_objects(app)
    design_layer.Visible = True
    colors_group.Visible = False
    background_group.Visible = False
    doc.Save()
    doc.Close()

    psd = PSDImage.open(psd_file_path)
    psd.composite().save('test.png')

test()
