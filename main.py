from urllib.parse import quote, unquote
from functools import lru_cache
from pathlib import Path
import traceback
import datetime
import zipfile
import shutil
import uuid
import time
import json
import os
import io
import re

from flask import Flask, send_file, abort, send_from_directory, jsonify, request, make_response, redirect
from werkzeug.utils import secure_filename
from PIL import Image
import jwt

from priority_thread_pool_executor import PriorityThreadPoolExecutor
import mockup_generator
import amazon_crawler
import model

class Cache:
    def __init__(self, maxsize=256):
        self.maxsize = maxsize
        self.call_history = []
        self.data = {}

    def cache(self, fn):
        def wrapper(*args, **kwargs):
            key = json.dumps({
                'args': args,
                'kwargs': kwargs
            })
            cached_result = self.data.get(key)
            if not cached_result:
                result = fn(*args, **kwargs)
                self.data[key] = result
                if key in self.call_history:
                    self.call_history.remove(key)
                self.call_history.append(key)
                if len(self.call_history) > self.maxsize:
                    remove_key = self.call_history[0]
                    self.call_history.pop(0)
                    self.data.pop(remove_key, None)
                return result
            return cached_result
        return wrapper
    
    def clear_input(self, *args, **kwargs):
        print(args)
        print(kwargs)
        key = json.dumps({
            'args': args,
            'kwargs': kwargs
        })
        self.call_history.remove(key)
        self.data.pop(key, None)
    
    def clear(self):
        self.call_history = []
        self.data = {}

app = Flask(__name__, static_url_path='/static')
executor = PriorityThreadPoolExecutor(max_workers=1)

def get_timestamp():
    return int(time.time() * 1000)

def validate_sku(sku):
    return not not re.match(r'^(?!.*?\-\-)[a-z0-9-_]+$', sku)

def safe_list_get(l, idx, default=None):
  try:
    return l[idx]
  except IndexError:
    return default

def create_folder(path):
    Path(path).mkdir(parents=True, exist_ok=True)

def serve_pil_image(pil_img):
    img_io = io.BytesIO()
    pil_img_jpg = pil_img.convert('RGB')
    pil_img_jpg.save(img_io, 'JPEG', quality=80, optimize=True, progressive=True)
    img_io.seek(0)
    return send_file(img_io, mimetype='image/jpeg')

merge_layers_cache = Cache(maxsize=64)
@merge_layers_cache.cache
def merge_layers(sku, psd_name, psd_color):
    print_layer_file_path = f'./designs/{sku}/print-layer/{psd_name}.png'
    base_layer_file_path = f'./psds/{psd_name}/__base-layer/{psd_color}.png'

    if not os.path.exists(print_layer_file_path) or not os.path.exists(base_layer_file_path):
        return None

    base_layer_image = Image.open(base_layer_file_path)
    print_layer_image = Image.open(print_layer_file_path)
    final_image = Image.new('RGBA',(base_layer_image.size[0], base_layer_image.size[1]), (0, 0, 0, 0))
    final_image.paste(base_layer_image, (0, 0))
    final_image.paste(print_layer_image, (0, 0), print_layer_image)

    base_layer_image.close()
    print_layer_image.close()

    return final_image

def invalidate_design_cache(design_name):
    for key in merge_layers_cache.call_history:
        arg_data = json.loads(key)
        if arg_data['args'][0] == design_name:
            merge_layers_cache.clear_input(*arg_data['args'], **arg_data['kwargs'])

class Task:
    version = uuid.uuid4()

    def __init__(self, fn, *args, **kwargs):
        self.record = model.Task.create(status='pending', description='', result='')
        self.fn = fn
        self.args = (self, *args)
        self.kwargs = kwargs
        self.future = None

    def execute(self):
        def job():
            try:
                self.set_status('running')
                self.fn(*self.args, **self.kwargs)
                self.set_status('finished')
            except Exception as e:
                self.set_status('error')
                self.set_result(str(e))
                traceback.print_exc()
                print(e)
        self.future = executor.submit(job)

    def set_description(self, description):
        self.record.description = description
        self.record.save()

    def set_status(self, status):
        if status in ['pending', 'running', 'finished', 'error']:
            self.record.status = status
            self.record.save()
        else:
            raise Exception('Invalid status provided')

    def set_result(self, result):
        result = str(result)
        self.record.result = result
        self.record.save()

    @classmethod
    def new_version(cls):
        cls.version = uuid.uuid4()

def job_create_print_layer(task, design_name):
    pass

def job_create_missing_print_layers(task):
    pass

@app.before_request
def before_request():
    if request.path == '/404':
        return abort(404)
    is_api_path = '/v2/api/' in request.path
    is_designs_path = '/v2/designs/' in request.path
    is_psds_path = '/v2/psds/' in request.path
    if is_api_path or is_designs_path or is_psds_path:
        try:
            access_token = request.cookies.get('_tat_mockup_access_token', '')
            content = jwt.decode(access_token, "secret", algorithms=["HS256"])
            message = content.get('message')
            if message != 'ThereIsNoSpoon':
                raise Exception('Incorrect message')
        except:
            if is_api_path:
                return jsonify({
                    'message': 'Unauthorized'
                }), 401
            return redirect(f'/v2/authenticate?backref={quote(request.url)}')

@app.route('/view', defaults={ 'path': 'index.html' })
@app.route('/view/', defaults={ 'path': 'index.html' })
@app.route('/view/<path:path>')
def send_view(path):
    return send_from_directory('public', path)

@app.route('/v2/authenticate', methods=['GET', 'POST'])
def routev2_authenticate():
    if request.method == 'GET':
        return send_from_directory('public', 'login.html')

    if not request.json:
        return abort(401)
    username = request.json.get('username')
    password = request.json.get('password')
    if not username:
        return jsonify({
            'message': "'username' is required"
        }), 400
    if not password:
        return jsonify({
            'message': "'password' is required"
        }), 400
    logged_in = False
    if username == 'admin' and password == 'theateam':
        logged_in = True
    elif username == 'n30' and password == 'thereisnospoon':
        logged_in = True
    elif username == 'n30':
        return jsonify({
            'message': "You're not him"
        }), 401
    if not logged_in:
        return jsonify({
            'message': 'Incorrect username/password'
        }), 401
    encoded = jwt.encode({
        'message': 'ThereIsNoSpoon'
    }, "secret", algorithm="HS256")
    response = make_response(encoded)
    response.set_cookie('_tat_mockup_access_token', value=encoded, httponly=True)
    return response

@app.route('/v2/logout', methods=['POST'])
def routev2_logout():
    response = jsonify({
        'message': 'Logged out'
    })
    response.set_cookie('_tat_mockup_access_token', value='', httponly=True)
    return response

@app.route('/v2/images/<filename>')
def routev2_images_filename(filename):
    filename = filename.split('.')[0]
    parts = filename.split('--')
    sku = safe_list_get(parts, 0)
    psd_name = safe_list_get(parts, 1)
    psd_color = safe_list_get(parts, 2)

    image = merge_layers(sku, psd_name, psd_color)

    if image == None:
        return abort(404)

    return serve_pil_image(image)

# admin
@app.route('/v2/psds/<name>')
def routev2_psds_name(name):
    name = name.split('.')[0]
    return send_from_directory('psds', f'{name}.zip')

# admin
@app.route('/v2/designs/<sku>')
def routev2_designs_filename(sku):
    sku = sku.split('.')[0]
    return send_from_directory(f'designs/{sku}', 'design.png')

@app.route('/v2/api/psd_upload', methods=['POST'])
def apiv2_psd_upload():
    if 'file' not in request.files:
        return jsonify({
            'message': 'No file part'
        }), 400
    file = request.files['file']
    name = request.form.get('name')
    if name and not validate_sku(name):
        return jsonify({
            'message': 'Provided name is invalid'
        }), 400
    if file.filename == '':
        return jsonify({
            'message': 'No selected file'
        }), 400
    ext = file.filename.split('.')[-1].lower()
    name = name if name else file.filename.split('.')[0].lower()
    psds = model.Psd.select().where(model.Psd.name == name)
    if len(psds) == 0:
        return jsonify({
            'message': 'Psd not found'
        }), 404
    fullname = f'{name}.{ext}'
    if file and ext == 'zip':
        filename = secure_filename(fullname)
        zip_path = os.path.join('./psds', filename)
        psd_path = f'./psds/{name}/mockup.psd'
        base_layer_path = f'./psds/{name}/__base-layer'
        file.save(zip_path)
        def generate_base_layer(task):
            mockup_generator.generate_base_layer(psd_path, base_layer_path)
            task.set_result(base_layer_path)
        with zipfile.ZipFile(zip_path, 'r') as zip_ref:
            zip_ref.extractall(f'./psds/{name}')
        task = Task(generate_base_layer)
        task.set_description(f"Generate base layer at '{base_layer_path}'")
        task.execute()
        return jsonify({
            'message': 'OK'
        })
    return jsonify({
        'message': 'File is not a zip'
    }), 400

@app.route('/v2/api/design_upload', methods=['POST'])
def apiv2_design_upload():
    if 'file' not in request.files:
        return jsonify({
            'message': 'No file part'
        }), 400
    file = request.files['file']
    name = request.form.get('name')
    if name and not validate_sku(name):
        return jsonify({
            'message': 'Provided name is invalid'
        }), 400
    if file.filename == '':
        return jsonify({
            'message': 'No selected file'
        }), 400
    ext = file.filename.split('.')[-1].lower()
    name = name if name else file.filename.split('.')[0].lower()
    designs = model.Design.select().where(model.Design.name == name)
    design = designs.first()
    if design == None:
        return jsonify({
            'message': 'Design not found'
        }), 404
    fullname = f'{name}.{ext}'
    if file and ext == 'png':
        filename = secure_filename(fullname)
        create_folder(f'./designs/{name}/print-layer')
        image_save_path = f'./designs/{name}/design.png'
        try:
            if os.path.exists(image_save_path):
                os.remove(image_save_path)
                time.sleep(0.5)
        except:
            pass
        file.save(image_save_path)
        psd_n_design_records = design.psdanddesigns
        psds = [psd_n_design.psd.name for psd_n_design in psd_n_design_records]
        create_folder(f'./designs/{name}/print-layer')
        for psd_name in psds:
            try:
                psd_path = f'./psds/{psd_name}/mockup.psd'
                design_image_path = image_save_path
                design_layer_save_path = f'./designs/{name}/print-layer/{psd_name}.png'
                def generate_design_layer(task):
                    mockup_generator.generate_design_layer(psd_path, design_image_path, design_layer_save_path)
                    task.set_result(design_layer_save_path)
                task = Task(generate_design_layer)
                task.set_description(f"Generate design layer at '{design_layer_save_path}'")
                task.execute()
            except Exception as e:
                print(e)
        invalidate_design_cache(name)
        return jsonify({
            'message': 'OK'
        })
    return jsonify({
        'message': 'File  is not a png'
    }), 400

@app.route('/v2/api/psds', methods=['GET', 'POST'])
def apiv2_psds():
    if request.method == 'GET':
        request_args = request.args or {}
        page        = int(request_args.get('page', 1))
        page_size   = int(request_args.get('page_size', 20))
        page_size   = max(page_size, 1)
        page_size   = min(page_size, 100)
        query       = model.Psd.select()
        # psds        = query.paginate(page, page_size)
        psds        = query
        count       = query.count()
        return jsonify({
            'data': [{
                'id': psd.id,
                'name': psd.name
            } for psd in psds],
            'total': count
        })
    request_json = request.json or {}
    name = request_json.get('name')
    if not name:
        return jsonify({
            'message': "'name' is required"
        }), 400
    psds = model.Psd.select().where(model.Psd.name == name)
    if len(psds) > 0:
        return jsonify({
            'message': 'Psd name already exists'
        }), 409
    if not validate_sku(name):
        return jsonify({
            'message': 'Name contains invalid characters'
        })
    model.Psd.create(name=name)
    create_folder(f'./psds/{name}/__base-layer')
    return jsonify({
        'message': 'Psd created'
    }), 201

@app.route('/v2/api/psds/<name>', methods=['DELETE'])
def apiv2_psds_name(name):
    model.PsdAndDesign.delete().where(model.PsdAndDesign.psd.name == name).execute()
    model.Psd.delete().where(model.Psd.name == name).execute()
    if os.path.exists(f'./psds/{name}'):
        shutil.rmtree(f'./psds/{name}')
    if os.path.exists(f'./psds/{name}.zip'):
        os.remove(f'./psds/{name}.zip')
    return jsonify({
        'message': 'Psd deleted'
    })

@app.route('/v2/api/designs', methods=['GET', 'POST'])
def apiv2_designs():
    if request.method == 'GET':
        request_args = request.args or {}
        page        = int(request_args.get('page', 1))
        page_size   = int(request_args.get('page_size', 20))
        page_size   = max(page_size, 1)
        page_size   = min(page_size, 100)
        query       = model.Design.select().order_by(model.Design.id.desc())
        designs     = query.paginate(page, page_size)
        count       = query.count()
        return jsonify({
            'data': [{
                'id': design.id,
                'name': design.name
            } for design in designs],
            'total': count
        })
    request_json = request.json or {}
    name = request_json.get('name')
    if not name:
        return jsonify({
            'message': "'name' is required"
        }), 400
    designs = model.Design.select().where(model.Design.name == name)
    if len(designs) > 0:
        return jsonify({
            'message': 'Design name already exists'
        }), 409
    if not validate_sku(name):
        return jsonify({
            'message': 'Name contains invalid characters'
        }), 400
    model.Design.create(name=name)
    create_folder(f'./designs/{name}/print-layer')
    return jsonify({
        'message': 'Design created'
    }), 201

@app.route('/v2/api/designs/<name>', methods=['GET', 'PUT', 'DELETE'])
def apiv2_designs_name(name):
    designs = model.Design.select().where(model.Design.name == name)
    if request.method == 'GET':
        if len(designs) == 0:
            return jsonify({
                'message': 'Design not found'
            }), 404
        design = designs[0]
        psd_n_design_records = design.psdanddesigns
        return jsonify({
            'data': {
                'name': design.name,
                'psds': [psd_n_design.psd.name for psd_n_design in psd_n_design_records]
            }
        })
    if request.method == 'PUT':
        if len(designs) == 0:
            return jsonify({
                'message': 'Design not found'
            }), 404
        design = designs[0]
        if request.json == None:
            return jsonify({
                'message': 'json body is required'
            }), 400
        psd_n_design_records = design.psdanddesigns
        existing_psds = [psd_n_design.psd.name for psd_n_design in psd_n_design_records]
        psds = request.json.get('psds')
        if type(psds) != list:
            return jsonify({
                'message': "'psds' is required"
            }), 400
        create_list = []
        remove_list = []
        for psd in psds:
            if psd in existing_psds:
                existing_psds.remove(psd)
            else:
                psd_records = model.Psd.select().where(model.Psd.name == psd)
                if len(psd_records) == 0:
                    return jsonify({
                        'message': f"Psd '{psd}' not found"
                    }), 404
                else:
                    psd_record = psd_records[0]
                    create_list.append(psd_record)
        for psd in existing_psds:
            psd_records = model.Psd.select().where(model.Psd.name == psd)
            if len(psd_records) == 0:
                return jsonify({
                    'message': f"Psd '{psd}' not found"
                }), 404
            else:
                psd_record = psd_records[0]
                remove_list.append(psd_record)
        # the part that make changes
        for psd_record in create_list:
            psd_name = psd_record.name
            model.PsdAndDesign.create(psd=psd_record.id, design=design.id)
            try:
                psd_path = f'./psds/{psd_name}/mockup.psd'
                design_image_path = f'./designs/{name}/design.png'
                design_layer_save_path = f'./designs/{name}/print-layer/{psd_name}.png'
                create_folder(f'./designs/{name}/print-layer')
                def generate_design_layer(task):
                    mockup_generator.generate_design_layer(psd_path, design_image_path, design_layer_save_path)
                    task.set_result(design_layer_save_path)
                task = Task(generate_design_layer)
                task.set_description(f"Generate design layer at '{design_layer_save_path}'")
                task.execute()
            except Exception as e:
                print(e)
        # another part that also make changes
        for psd_record in remove_list:
            psd_name = psd_record.name
            model.PsdAndDesign.delete().where(model.PsdAndDesign.psd == psd_record.id).execute()
            try:
                image_file_path = f'./designs/{name}/print-layer/{psd_name}.png'
                if os.path.exists(image_file_path):
                    os.remove(image_file_path)
            except:
                pass
        invalidate_design_cache(name)
        return jsonify({
            'message': 'Design updated'
        })
    if len(designs) == 0:
        return jsonify({
            'message': 'Design deleted'
        })
    design = designs[0]
    query = model.PsdAndDesign.delete().where(model.PsdAndDesign.design == design.id)
    query.execute()
    model.Design.delete().where(model.Design.name == name).execute()
    if os.path.exists(f'./designs/{name}'):
        shutil.rmtree(f'./designs/{name}')
    return jsonify({
        'message': 'Design deleted'
    })

@app.route('/v2/api/tasks', methods=['GET'])
def apiv2_tasks():
    request_args = request.args or {}
    page        = int(request_args.get('page', 1))
    page_size   = int(request_args.get('page_size', 20))
    page_size   = max(page_size, 1)
    page_size   = min(page_size, 100)
    query       = model.Task.select().order_by(model.Task.id.desc())
    tasks       = query.paginate(page, page_size)
    count       = query.count()
    return jsonify({
        'data': [{
            'id': task.id,
            'status': task.status,
            'description': task.description,
            'result': task.result
        } for task in tasks],
        'total': count
    })

@app.route('/v2/api/active_tasks', methods=['GET'])
def apiv2_active_tasks():
    request_args = request.args or {}
    page        = int(request_args.get('page', 1))
    page_size   = int(request_args.get('page_size', 20))
    page_size   = max(page_size, 1)
    page_size   = min(page_size, 100)
    running_tasks = model.Task.select().where(model.Task.status == 'running').order_by(model.Task.id.desc())
    pending_tasks = model.Task.select().where(model.Task.status == 'pending').order_by(model.Task.id.desc())
    tasks = []
    for task in running_tasks:
        tasks.append(task)
    for task in pending_tasks:
        tasks.append(task)
    return jsonify({
        'data': [{
            'id': task.id,
            'status': task.status,
            'description': task.description,
            'result': task.result
        } for task in tasks]
    })

@app.route('/v2/api/tasks/<taskid>', methods=['GET', 'DELETE'])
def apiv2_tasks_taskid(taskid):
    if request.method == 'GET':
        tasks = model.Task.select().where(model.Task.id == taskid)
        if len(tasks) == 0:
            return jsonify({
                'message': 'Task not found'
            }), 404
        task = tasks[0]
        return jsonify({
            'data': {
                'id': task.id,
                'status': task.status,
                'description': task.description,
                'result': task.result
            }
        })
    # wip
    return jsonify({
        'message': 'wip'
    })

@app.route('/v2/api/actions/generate_print_layer', methods=['POST'])
def apiv2_actions_generate_print_layer():
    pass

@app.route('/v2/api/actions/task_cancel', methods=['POST'])
def apiv2_actions_task_cancel():
    pass

@app.route('/v2/api/actions/amazon_clones', methods=['POST'])
def apiv2_actions_amazon_clones():
    pass

@app.route('/v2/api/actions/generate_missing_print_layers', methods=['POST'])
def apiv2_actions_generate_missing_print_layers():
    pass

@app.route('/v2/api/actions/invalidate_tasks', methods=['POST'])
def apiv2_actions_invalidate_tasks():
    pass

################################################################################################################################
# OLD                                                                                                                          #
################################################################################################################################

@lru_cache(maxsize=128)
def generate_image(mockup_name, mockup_variant, design_name, width):
    PRINT_AREA_CONFIGS = {
        'default': {
            'left': 0.28,
            'top': 0.2,
            'width': 0.45,
            'height': 0.45
        },
        'men-tshirt-front': {
            'left': 0.355,
            'top': 0.35,
            'width': 0.260,
            'height': 0.3
        },
        'women-tshirt-front': {
            'left': 0.375,
            'top': 0.35,
            'width': 0.25,
            'height': 0.25
        },
        'kid-tshirt': {
            'left': 0.3,
            'top': 0.25,
            'width': 0.4,
            'height': 0.4
        },
        'kid-tshirt-front': {
            'left': 0.4,
            'top': 0.45,
            'width': 0.25,
            'height': 0.25
        }
    }
    try:
        print_area_config = PRINT_AREA_CONFIGS.get(mockup_name, PRINT_AREA_CONFIGS['default'])
        start_timestamp = time.time()
        base_mockup = Image.open(f'base-mockups/{mockup_name}/{mockup_variant}.jpg')
        design_image = Image.open(f'designs/{design_name}.png')
        base_mockup_size = base_mockup.size
        design_image_size = design_image.size
        max_design_width = base_mockup_size[0] * print_area_config['width']
        max_design_height = base_mockup_size[1] * print_area_config['height']
        design_image_scale_ratio = min(max_design_width / design_image_size[0], max_design_height / design_image_size[1])
        new_image = Image.new('RGBA',(base_mockup_size[0], base_mockup_size[1]), (0, 0, 0, 0))
        new_image_2 = Image.new('RGBA',(base_mockup_size[0], base_mockup_size[1]), (0, 0, 0, 0))
        new_image.paste(base_mockup, (0,0))
        new_image_2.paste(base_mockup, (0,0))
        design_image_new_size = (
            int(design_image.size[0] * design_image_scale_ratio),
            int(design_image.size[1] * design_image_scale_ratio)
        )
        design_image_offset = (
            int((max_design_width - design_image_new_size[0]) / 2),
            int((max_design_height - design_image_new_size[1]) / 2)
        )
        design_image = design_image.resize(design_image_new_size)
        paste_position = (
            int(new_image.size[0] * print_area_config['left'] + design_image_offset[0]),
            int(new_image.size[0] * print_area_config['top'] + design_image_offset[0]),
        )
        new_image.paste(design_image, paste_position, design_image)
        new_image_3 = Image.blend(new_image_2, new_image, 0.9)

        finish_timestamp = time.time()
        execution_time = (finish_timestamp - start_timestamp) * 1000
        print(f'Execution time {round(execution_time, 1)} millisecs')

        if width >= new_image_3.size[0]:
            return new_image_3
        resized_size = (
            width,
            int(width / new_image_3.size[0] * new_image_3.size[1])
        )
        new_image_3.thumbnail(resized_size, Image.ANTIALIAS)

        return new_image_3
    except:
        return None

@lru_cache(maxsize=128)
def resize_image(filepath, width):
    image = Image.open(filepath)
    image_size = image.size
    if width >= image_size[0]:
        return image
    new_size = (
        width,
        int(width / image_size[0] * image_size[1])
    )
    image.thumbnail(new_size, Image.ANTIALIAS)
    return image

@app.route('/demo', defaults={ 'path': 'index.html' })
@app.route('/demo/', defaults={ 'path': 'index.html' })
@app.route('/demo/<path:path>')
def send_demo(path):
    return send_from_directory('demo', path)

@app.route('/mockups/<path:path>')
def send_mockups(path):
    return send_from_directory('mockups', path)

@app.route('/base-mockups/<filename>')
def serve_base_mockup(filename):
    resize_width = int(request.args.get('width', 800))
    img_io = io.BytesIO()
    if os.path.exists(f'base-mockups/{filename}.jpg'):
        pil_img = resize_image(f'base-mockups/{filename}.jpg', resize_width)
        pil_img_jpg = pil_img.convert('RGB')
        pil_img_jpg.save(img_io, 'JPEG', quality=80, optimize=True, progressive=True)
        img_io.seek(0)
        return send_file(img_io, mimetype='image/jpeg')
    if os.path.exists(f'base-mockups/{filename}.png'):
        pil_img = resize_image(f'base-mockups/{filename}.png', resize_width)
        pil_img_jpg = pil_img.convert('RGB')
        pil_img_jpg.save(img_io, 'JPEG', quality=80, optimize=True, progressive=True)
        img_io.seek(0)
        return send_file(img_io, mimetype='image/jpeg')
    return abort(404)

# @app.route('/mockups/<filename>')
# def serve_img(filename):
#     mockup = filename.replace('.jpg', '').replace('.png', '').split('--')
#     if len(mockup) < 3:
#         return abort(400)
#     [mockup_name, mockup_variant, design_name] = mockup
#     resize_width = int(request.args.get('width', 800))
#     img = generate_image(mockup_name, mockup_variant, design_name, resize_width)
#     if not img:
#         return abort(404)
#     return serve_pil_image(img)

@app.route('/api/mockups/<mockup_name>')
def serve_api_mockups_mockup_name(mockup_name):
    result = []
    for mockup_type in os.listdir('./mockups'):
        mockup_path = f'./mockups/{mockup_type}/{mockup_name}.jpg'
        if os.path.exists(mockup_path):
            result.append(f'/mockups/{mockup_type}/{mockup_name}.jpg')
    return jsonify({
        'data': result
    })

# @app.route('/api/generate_missing_mockups', methods=['POST'])
# def serve_api_generate_missing_mockups():
#     pass

# @app.route('/api/mockups', methods=['GET', 'POST'])
# def serve_api_mockups():
#     if request.method == 'GET':
#         query = model.Mockup.select()
#         result = []
#         for mockup in query:
#             result.append({
#                 'id': mockup.id,
#                 'name': mockup.name,
#                 'left': mockup.left,
#                 'top': mockup.top,
#                 'width': mockup.width,
#                 'height': mockup.height
#             })
#         return jsonify(result)
#     if request.method == 'POST':
#         try:
#             model.Mockup.create(
#                 name    = request.json['name'],
#                 left    = request.json['left'],
#                 top     = request.json['top'],
#                 width   = request.json['width'],
#                 height  = request.json['height']
#             )
#             return 'Created', 201
#         except Exception as e:
#             raw_error = str(e)
#             formatted_error = raw_error
#             if raw_error == "UNIQUE constraint failed: mockup.name":
#                 formatted_error = 'Mockup name already exists'
#                 return jsonify({ 'message': formatted_error }), 400
#             return jsonify({ 'message': raw_error }), 500
#     return abort(405)

# @app.route('/api/designs')
# def serve_api_designs():
#     files = sorted(os.listdir('./designs'))
#     return jsonify([file.replace('.png', '') for file in files])

def amz_download_and_generate_mockup(url, name):
    def create_folder(path):
        Path(path).mkdir(parents=True, exist_ok=True)
    design_file_path = f'./designs/{name}_1.png'
    if not os.path.exists(design_file_path):
        amazon_crawler.crawl(url, name)
    for psd_mockup in os.listdir('./psds'):
        mockup_file_path = f'./mockups/{psd_mockup}/{name}.jpg'
        create_folder(f'./mockups/{psd_mockup}')
        if os.path.exists(design_file_path) and not os.path.exists(mockup_file_path):
            try:
                mockup_generator.generate_mockup(f'./psds/{psd_mockup}/mockup.psd', f'./designs/{name}_1.png', mockup_file_path)
            except Exception as e:
                traceback.print_exc()
                print(e)
    print('Done generate')

@app.route('/api/amazon-clones', methods=['POST'])
def serve_api_amazon_clones():
    if request.json == None:
        return jsonify({
            'message': 'json body is required'
        }), 400
    url = request.json.get('url')
    name = request.json.get('name')
    if not url:
        return jsonify({
            'message': "'url' is required"
        }), 400
    if not name:
        return jsonify({
            'message': "'name' is required"
        }), 400
    executor.submit(amz_download_and_generate_mockup, url, name)
    return jsonify({
        'url': url,
        'name': name
    })

def main():
    tasks = model.Task.select()
    for task in tasks:
        if task.status != 'finished' and task.status != 'error':
            task.status = 'error'
            task.result = 'Task corrupted'
            task.save()
main()

app.run(host="localhost", port="8000", debug=True)

