import requests
from lxml.html import fromstring

if __name__ == '__main__':
    url = 'https://www.teeshirtpalace.com/products/rfk6599520-roy-freaking-kent-vintage-toddler-t-shirt'
    response = requests.get(url)
    document = fromstring(response.text)
    print(document.get_element_by_id('backend').get('data-designdata'))
